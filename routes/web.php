<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Inde

Route::get('/', 'Home\HomeController@getIndex')->name('getIndex');


Route::group(['middleware' => 'login.register'], function (){
    //login
    Route::get('login', 'Auth\LoginController@getLogin')->name('getLogin');
    Route::post('login', 'Auth\LoginController@postLogin')->name('postLogin');
    //register
    Route::get('register', 'Auth\RegisterController@getRegister')->name('getRegister');
    Route::post('register', 'Auth\RegisterController@postRegister')->name('postRegister');
    //active
    Route::get('active', 'Auth\RegisterController@getActive')->name('getActiveAccount');
    Route::post('loginCheckOTP','Auth\LoginController@postLoginCheckOTP')->name('postLoginCheckOTP');
    //forgot
    Route::get('forgot-password', 'Auth\ForgotPasswordController@getForgotPassword')->name('getForgotPassword');
    Route::post('forgot-password', 'Auth\ForgotPasswordController@postForgotPassword')->name('postForgotPassword');
});

Route::group(['prefix' => 'system', 'middleware'=>'login'], function () {

    //Dashboard
    Route::get('/', 'System\DashboardController@getDashboard')->name('system.getDashboard');
    Route::get('logout', 'Auth\LoginController@getLogout')->name('getLogout');

    //Profile
    Route::get('profile', 'System\UserController@getProfile')->name('getProfile');
    Route::post('change-password', 'Auth\ResetPasswordController@changePassword')->name('system.changePassword');
    Route::post('post-walletl-address', 'System\UserController@postWalletAddress')->name('system.postWalletAddress');
//    Route::get('confirm-personal-info', 'System\UserController@getConfirmPersonalInfo')->name('getConfirmPersonalInfo');
//    Route::post('confirm-personal-info', 'System\UserController@postConfirmPersonalInfo')->name('postConfirmPersonalInfo');
    Route::post('auth', 'System\UserController@postAuth')->name('system.postAuth');
    Route::post('post-kyc', 'System\UserController@postKYC')->name('system.postKYC');

    //members
    Route::get('get-member', 'System\UserController@getMember')->name('system.getMember');
    Route::get('get-tree/{id?}', 'System\UserController@getTree')->name('system.getTree');
    //Wallet
    Route::get('wallet', 'System\WalletController@getWallet')->name('system.getWallet');
    //Investment
    Route::get('investment', 'System\InvestmentController@getInvestment')->name('system.getInvestment');
    //ticket
    Route::get('ticket', 'System\TicketController@getTicket')->name('system.getTicket');
    //history
    Route::group(['prefix'=>'history'], function (){
        Route::get('wallet', 'System\HistoryController@getWallet')->name('system.history.getWallet');
        Route::get('commission', 'System\HistoryController@getCommisson')->name('system.history.getCommission');
        Route::get('investment', 'System\HistoryController@getInvestment')->name('system.history.getInvestment');
    });

    //admin
    Route::group(['prefix'=>'admin'], function () {
        //getUser
        Route::get('get-users', 'System\UserController@getUsers')->name('system.admin.getUsers');
        //Wallet
        Route::get('wallet', 'System\AdminController@getWallet')->name('system.admin.getWallet');
        //Investment
        Route::get('investment', 'System\AdminController@getInvestment')->name('system.admin.getInvestment');
        //Statictis
        Route::get('statistical', 'System\AdminController@getStatistic')->name('system.admin.getStatistic');

        //support
        Route::group(['prefix'=>'support'], function () {
            Route::get('log-mail', 'System\SupportController@logEmail')->name('system.admin.support.logEmail');
            Route::get('active-email', 'System\SupportController@activeEmail')->name('system.admin.support.activeEmail');
            Route::get('off-auth', 'System\SupportController@offAuth')->name('system.admin.support.offAuth');
        });

        Route::group(['prefix'=>'confirm'], function () {
            //KYC
            Route::get('kyc', 'System\AdminController@getKYC')->name('system.admin.confirm.getKYC');
            //Withdraw
            Route::get('withdraw', 'System\AdminController@getWithdraw')->name('system.admin.confirm.getWithdraw');
            //Interest
            Route::get('interest', 'System\AdminController@getInterest')->name('system.admin.confirm.getInterest');
        });
        //Support
        Route::get('support', 'System\AdminController@getSupport')->name('system.admin.getSupport');
        //Detail Support
        Route::get('detailSupport', 'System\AdminController@getDetailsp')->name('system.admin.getDetailsp');
    });

});

Route::get('test', 'TestController@getIndex');

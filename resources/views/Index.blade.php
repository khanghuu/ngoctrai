<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title id="title">BlueSea</title>
		<meta name="description" content="Porky is a responsive HTML5 coming soon template by Hencework." />
		<meta name="keywords" content="Porky, responsive coming soon template, under construction template, multipurpose, responsive template, html5 template, themeforest.net, bootstrap, html5, creative, landing page, sass, clean, design, modern, angular js, mailchimp subscription," />
		<meta name="author" content="Hencework"/>
		<base href="{{asset('')}}">
		<link rel="shortcut icon" href="img/iconlogoBluesea.png">
		<link rel="icon" href="img/iconlogoBluesea.png" type="image/x-icon">

		<!--Fonts-->
		<link href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Lora:400,400i,700,700i' rel='stylesheet' type='text/css'>

		<!--CSS-->
		<link rel="stylesheet" type="text/css" href="css/style.css" />
	</head>

	<body data-ng-app="contactApp">

		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->

		<!--Container-->
		<div class="pad-zero">

			<!--Splitlayout -->
			<div id="splitlayout" class="splitlayout reset-layout">

				<!--Intro-->
				<div class="intro">

					<!--Left Content Intro-->
					<div class="side side-left text-center" style="width: 100%">


						<!--/Left Nav-->

						<!--Main Content-->
						<section class="col-lg-12 content-wrap align-center sec-pad">
							<div class="table-wrap">
								<div class="table-cell vertical-align-middle">
									<!--Logo--><br><br>
									<header class="logo margin-bottom-25">
										<div id="calltrap-btn" class="logoBluesea row b-calltrap-btn calltrap_offline hidden-phone visible-tablet">
											<div id="calltrap-ico">
												<img src="img/logoBluesea.png" alt="logo">
											</div>
										</div>
									</header>
<br><br>
									<div class="type-wrap hide-overflow margin-bottom-15">
										<h1 class="animated"><span id="typed" ></span></h1>
									</div>
								</div>
							</div>
							<!--left overlay-->
						</section>

						<!--Kenburn Image-->
						<div id="parallax_img" class="left top"></div>
						<!--/Kenburn Image-->

						<!--Snow Effect-->
						<div id="snow"></div>
						<!--/Snow Effect-->

						<!--Left overlay-->
						<div class="side-left-overlay"></div>
						<!--Left overlay-->

					</div>
					<!--/Left Content Intro-->

					<!--Right Content Intro-->

					<!--/Right Content Intro-->

				</div>
				<!--/Intro-->

				<!--Left Section-->

				<!--/Left Section-->

				<!--Right Section-->

				<!--/Right Section-->

			</div>
			<!-- /Splitlayout -->

		</div>
		<!-- /Container -->

		<!--PhotoSwipe Popup-->

		<!--/PhotoSwipe Popup-->

		<!-- Scripts -->
		<script src="js/jquery-1.11.3.min.js"></script>
		<script src="js/jquery-ui.min.js"></script>
		<script src="js/angular.min.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<script src="js/cbpSplitLayout.js"></script>
		<script src="js/app.js"></script>
		<script src="js/controllers.js"></script>
		<script src="js/notifyMe.js"></script>
		<script src="js/jquery.placeholder.js"></script>
		<script src="js/photoswipe.min.js"></script>
		<script src="js/photoswipe-ui-default.min.js"></script>
		<script src="js/modernizr-2.6.2.min.js"></script>
		<script src="js/jquery.nicescroll.js"></script>
		<script src="js/jquery.countdown.js"></script>
		<script src="js/jquery.parallaxmouse.min.js"></script>
		<script src="js/ThreeCanvas.js"></script>
		<script src="js/snow.js"></script>
		<script src="js/typed.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxXt2P7-U38bK0xEFIT-ebZJ1ngK8wjww"></script>
		<script src="js/init.js"></script>

	</body>
</html>

@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb hk-margin" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item"><a href="#">Members</a></li>
            <li class="breadcrumb-item active" aria-current="page">Members Tree</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <div class="hk-row">
            <div class="col-lg-12">
                <div class="hk-row">
                    <div class="col-sm-12">
                        <div class="card-group hk-dash-type-2">
                            <div class="card card-sm">
                                <div class="card-header card-header-action header_height">

                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-10">
                                        <div>
                                            <p class="d-block font-30 text-white font-weight-500 index_name">Ur Link href
                                                <span class="card_index_dec"></span>
                                            </p>

                                        </div>

                                    </div>
                                    <div>
                                        <p class="d-block font-27 text-white mb-5 mt-35 link-href">ur link</p>

                                        <Button class="btn btn-primary float-right">Copy</Button>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-sm">
                                <div class="card-header card-header-action header_height">

                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-10">
                                        <div>
                                            <p class="d-block font-30 text-white font-weight-500 index_name">Add New Member
                                                <span class="card_index_dec"></span>
                                            </p>

                                        </div>

                                    </div>
                                    <div>
                                        <div class="col-md-6 mb-10">
                                            <input type="text" class="form-control AuthPage" id="validationCustom03" placeholder="Email" required>
                                            <div class="invalid-feedback">
                                                Please provide a valid city.
                                            </div>
                                        </div>
                                        <Button class="btn btn-primary float-right">Confirm</Button>
                                        <Button class="btn btn-primary float-right">Cancer</Button>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12">


                <div class="card card-refresh">
                    <div class="refresh-container">
                        <div class="loader-pendulums"></div>
                    </div>
                    <div class="card-header card-header-action">
                        <h6>Member Tree</h6>

                    </div>
                    <div class="card-body">

                    </div>
                </div>




            </div>
        </div>

    </div>
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection
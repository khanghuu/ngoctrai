@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb hk-margin" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item"><a href="#">Members</a></li>
            <li class="breadcrumb-item active" aria-current="page">Members List</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="hk-row">
            <div class="col-lg-12">
                <div class="hk-row">
                    <div class="col-sm-12">
                        <div class="card-group hk-dash-type-2">
                            <div class="card card-sm">
                                <div class="card-header card-header-action header_height">

                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-10">

                                        <div class="form-row">

                                            <p class="d-block font-30 text-white font-weight-500 index_name">Ur Link href
                                                <span class="card_index_dec"></span>
                                            </p>
                                            <input type="text" class="form-control  text-white mt-20" id="validationCustom03" value="text" readlink>
                                            <button class="btn btn-primary mt-20">Copy</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-sm">
                                <div class="card-header card-header-action header_height">

                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-10">
                                        <div>
                                            <p class="d-block font-30 text-white font-weight-500 index_name">Add New Member
                                                <span class="card_index_dec"></span>
                                            </p>

                                        </div>

                                    </div>
                                    <div>

                                        <input type="text" class="form-control text-white mb-30" id="validationCustom03" placeholder="Email" required>

                                    </div>
                                    <Button class="btn btn-primary float-right ml-10">Cancer</Button>
                                    <Button class="btn btn-primary float-right">confirm</Button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title text-white">Title</h5>

                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                                <th>Role</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody_backgound">
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Jens</td>
                                                <td>Brincker</td>
                                                <td>Brincker123</td>
                                                <td><span class="badge badge-danger">admin</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Mark</td>
                                                <td>Hay</td>
                                                <td>Hay123</td>
                                                <td><span class="badge badge-info">member</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Anthony</td>
                                                <td>Davie</td>
                                                <td>Davie123</td>
                                                <td><span class="badge badge-warning">developer</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td>David</td>
                                                <td>Perry</td>
                                                <td>Perry123</td>
                                                <td><span class="badge badge-success">supporter</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">5</th>
                                                <td>Anthony</td>
                                                <td>Davie</td>
                                                <td>Davie123</td>
                                                <td><span class="badge badge-info">member</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">6</th>
                                                <td>Alan</td>
                                                <td>Gilchrist</td>
                                                <td>Gilchrist123</td>
                                                <td><span class="badge badge-success">supporter</span> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection
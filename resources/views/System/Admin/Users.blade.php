@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb hk-margin" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active" aria-current="page">Users</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="hk-row">
            <div class="col-lg-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Custom Bootstrap Form Validation</h5>
                    <div class="row">
                        <div class="col-sm">
                            <form class="needs-validation" novalidate>
                                <div class="form-row">
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom01">First name</label>
                                        <input type="text" class="form-control AuthPage" id="validationCustom01" placeholder="First name" value="Mark" required>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom02">Last name</label>
                                        <input type="text" class="form-control AuthPage" id="validationCustom02" placeholder="Last name" value="Otto" required>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>



                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">City</label>
                                        <input type="text" class="form-control AuthPage" id="validationCustom03" placeholder="City" required>
                                        <div class="invalid-feedback">
                                            Please provide a valid city.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom04">State</label>
                                        <input type="text" class="form-control AuthPage" id="validationCustom04" placeholder="State" required>
                                        <div class="invalid-feedback">
                                            Please provide a valid state.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom05">Zip</label>
                                        <input type="text" class="form-control AuthPage" id="validationCustom05" placeholder="Zip" required>
                                        <div class="invalid-feedback">
                                            Please provide a valid zip.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-10">

                                        <label for="validationCustom03">Authen</label>
                                        <input type="text" class="form-control AuthPage" id="validationCustom03" placeholder="authen" required>
                                        <div class="invalid-feedback">
                                            Please provide a valid city.
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6 mb-10">
                                        <div class="form-group">
                                            <div class="form-check custom-control custom-checkbox">
                                                <input type="checkbox" class="form-check-input custom-control-input" id="invalidCheck" required>
                                                <label class="form-check-label custom-control-label" for="invalidCheck">
                                                    Agree to terms and conditions
                                                </label>
                                                <div class="invalid-feedback">
                                                    You must agree before submitting.
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-10">
                                        <button class="btn btn-primary float-right" type="submit">Submit form</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>

            </div>
            <div class="col-lg-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title text-white">Title</h5>

                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                                <th>Role</th>
                                            </tr>
                                        </thead>
                                        <tbody class="tbody_backgound">
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Jens</td>
                                                <td>Brincker</td>
                                                <td>Brincker123</td>
                                                <td><span class="badge badge-danger">admin</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>Mark</td>
                                                <td>Hay</td>
                                                <td>Hay123</td>
                                                <td><span class="badge badge-info">member</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>Anthony</td>
                                                <td>Davie</td>
                                                <td>Davie123</td>
                                                <td><span class="badge badge-warning">developer</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td>David</td>
                                                <td>Perry</td>
                                                <td>Perry123</td>
                                                <td><span class="badge badge-success">supporter</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">5</th>
                                                <td>Anthony</td>
                                                <td>Davie</td>
                                                <td>Davie123</td>
                                                <td><span class="badge badge-info">member</span> </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">6</th>
                                                <td>Alan</td>
                                                <td>Gilchrist</td>
                                                <td>Gilchrist123</td>
                                                <td><span class="badge badge-success">supporter</span> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection
@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item active" aria-current="page">Investment</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="hk-row">
            <div class="col-lg-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Custom Bootstrap Form Validation</h5>
                    <div class="row">
                        <div class="col-sm">
                            <form class="needs-validation" novalidate>
                                <div class="form-row">
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom01">UserID</label>
                                        <input type="text" class="form-control text-white" id="validationCustom01" placeholder="First name" value="Mark" readonly>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom02">UserName</label>
                                        <input type="text" class="form-control  text-white" id="validationCustom02" placeholder="Last name" value="Otto" readonly>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>



                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom03">Date</label>
                                        <input type="text" class="form-control  text-white" id="validationCustom03" placeholder="City" readonly>
                                        <div class="invalid-feedback">
                                            Please provide a valid city.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom04">Problem</label>
                                        <input type="text" class="form-control text-white" id="validationCustom04" placeholder="State" readonly>
                                        <div class="invalid-feedback">
                                            Please provide a valid state.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-10">
                                        <label for="validationCustom05">Descaption</label>
                                        <input type="text" class="form-control text-white" id="validationCustom05" placeholder="Zip" readonly>
                                        <div class="invalid-feedback">
                                            Please provide a valid zip.
                                        </div>
                                    </div>

                                </div>

                                <div class="form-row">

                                    <div class="col-md-12 mb-10">
                                        <button class="btn btn-success " type="submit">
                                            processed</button>
                                        <button class="btn btn-info  " type="submit">Send Mail</button>
                                        <a href="{{route('system.admin.getSupport')}}" class=" btn btn-danger float-right " type=" submit">Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>

            </div>

        </div>
    </div>
    <!-- /Container -->
    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection
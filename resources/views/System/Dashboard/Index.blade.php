@extends('System.Layouts.Master');

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->
    <!-- Container -->
    <div class="container mt-xl-50 mt-sm-30 mt-15">
        <!-- Title -->
        <div class="hk-pg-header align-items-top">
            <div>
                <h2 class="hk-pg-title font-weight-600 mb-10">Management</h2>

            </div>

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <div class="hk-row">
                    <div class="col-sm-12">
                        <div class="card-group hk-dash-type-2">
                            <div class="card card-sm">
                                <div class="card-header card-header-action header_height">

                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div>
                                            <p class="d-block font-15 text-white font-weight-500 index_name">Users
                                                <span class="card_index_dec"></span>
                                            </p>

                                        </div>
                                        <div class="card_index_percen">
                                            <span class="text-success font-14 font-weight-500">+10%</span>
                                        </div>
                                    </div>
                                    <div>
                                        <span class="d-block display-4 text-white mb-5">168M</span>
                                        <small class="d-block ">172,458 Target Users</small>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-sm">
                                <div class="card-header card-header-action header_height">

                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div>
                                            <p class="d-block font-15 text-white font-weight-500 index_name">Users
                                                <span class="card_index_dec"></span>
                                            </p>

                                        </div>
                                        <div class="card_index_percen">
                                            <span class="text-success font-14 font-weight-500">+10%</span>
                                        </div>
                                    </div>
                                    <div>
                                        <span class="d-block display-4 text-white mb-5">168M</span>
                                        <small class="d-block ">172,458 Target Users</small>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-sm">
                                <div class="card-header card-header-action header_height">

                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div>
                                            <p class="d-block font-15 text-white font-weight-500 index_name">Users
                                                <span class="card_index_dec"></span>
                                            </p>

                                        </div>
                                        <div class="card_index_percen">
                                            <span class="text-red font-14 font-weight-500">+10%</span>
                                        </div>
                                    </div>
                                    <div>
                                        <span class="d-block display-4 text-white mb-5">168M</span>
                                        <small class="d-block ">172,458 Target Users</small>
                                    </div>
                                </div>
                            </div>
                            <div class="card card-sm">
                                <div class="card-header card-header-action header_height">

                                </div>
                                <div class="card-body">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div>
                                            <p class="d-block font-15 text-white font-weight-500 index_name">Users
                                                <span class="card_index_dec"></span>
                                            </p>

                                        </div>
                                        <div class="card_index_percen">
                                            <span class="text-red font-14 font-weight-500">+10%</span>
                                        </div>
                                    </div>
                                    <div>
                                        <span class="d-block display-4 text-white mb-5">168M</span>
                                        <small class="d-block ">172,458 Target Users</small>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="hk-row">
                    <div class="col-lg-6">
                        <div class="card card-refresh">
                            <div class="refresh-container">
                                <div class="loader-pendulums"></div>
                            </div>
                            <div class="card-header card-header-action">
                                <h6>Youtube Subscribers</h6>

                            </div>
                            <div class="card-body">

                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6">
                        <section class="hk-sec-wrapper">
                            <h5 class="hk-sec-title text-white">Title</h5>
                          
                            <div class="row">
                                <div class="col-sm">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                        <th>Role</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tbody_backgound">
                                                    <tr>
                                                        <th scope="row">1</th>
                                                        <td>Jens</td>
                                                        <td>Brincker</td>
                                                        <td>Brincker123</td>
                                                        <td><span class="badge badge-danger">admin</span> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">2</th>
                                                        <td>Mark</td>
                                                        <td>Hay</td>
                                                        <td>Hay123</td>
                                                        <td><span class="badge badge-info">member</span> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">3</th>
                                                        <td>Anthony</td>
                                                        <td>Davie</td>
                                                        <td>Davie123</td>
                                                        <td><span class="badge badge-warning">developer</span> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">4</th>
                                                        <td>David</td>
                                                        <td>Perry</td>
                                                        <td>Perry123</td>
                                                        <td><span class="badge badge-success">supporter</span> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">5</th>
                                                        <td>Anthony</td>
                                                        <td>Davie</td>
                                                        <td>Davie123</td>
                                                        <td><span class="badge badge-info">member</span> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">6</th>
                                                        <td>Alan</td>
                                                        <td>Gilchrist</td>
                                                        <td>Gilchrist123</td>
                                                        <td><span class="badge badge-success">supporter</span> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

    <!-- Footer -->
    @include('System.Layouts.Footer');
    <!-- /Footer -->

</div>

@endsection
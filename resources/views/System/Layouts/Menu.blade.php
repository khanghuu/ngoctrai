<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">

            <div class="nav-header">
                <span>User Interface</span>
                <span>UI</span>
            </div>
            <ul class="navbar-nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('System.getDashboard')}}">
                        <span class="feather-icon"><i data-feather="layout"></i></span>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('system.getWallet')}}">
                        <span class="feather-icon"><i data-feather="anchor"></i></span>
                        <span class="nav-link-text">Wallet</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('system.getInvestment')}}">
                        <span class="feather-icon"><i data-feather="list"></i></span>
                        <span class="nav-link-text">Investment</span>
                    </a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#content_drp">
                        <span class="feather-icon"><i data-feather="user"></i></span>
                        <span class="nav-link-text">Member</span>
                    </a>
                    <ul id="content_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('system.getMember')}}">Members List</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('system.getTree')}}">Members Tree</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#maps_drp">
                        <span class="feather-icon"><i data-feather="clock"></i></span>
                        <span class="nav-link-text">History</span>
                    </a>
                    <ul id="maps_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('system.history.getWallet')}}">Wallet</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('system.history.getInvestment')}}">Investment</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('system.history.getCommission')}}">Commission</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="{{route('system.getTicket')}}">
                        <span class="feather-icon"><i data-feather="message-square"></i></span>
                        <span class="nav-link-text">Support</span>
                       
                    </a>
                </li>
            </ul>

            <hr class="nav-separator">
            <div class="nav-header">
                <span>Admin System</span>
                <span>GS</span>
            </div>
            <ul class="navbar-nav flex-column">
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="{{route('system.admin.getStatistic')}}">
                        <i class="material-icons">bar_chart</i>
                        <span class="nav-link-text">Statictis</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="{{route('system.admin.getWallet')}}">
                        <i class="material-icons">account_balance_wallet</i>
                        <span class="nav-link-text">Wallet</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="{{route('system.admin.getInvestment')}}">
                        <i class="material-icons">account_balance</i>
                        <span class="nav-link-text">Investment</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="{{route('system.admin.getUsers')}}">
                        <i class="material-icons">account_circle</i>
                        <span class="nav-link-text">Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#forms_drp">
                        <i class="material-icons">check_box</i>
                        <span class="nav-link-text">Confirm</span>
                    </a>
                    <ul id="forms_drp" class="nav flex-column collapse collapse-level-1">
                        <li class="nav-item" href="javascript:void(0);">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('system.admin.confirm.getKYC')}}">KYC</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('system.admin.confirm.getWithdraw')}}">Withdraw</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('system.admin.confirm.getInterest')}}">Interest</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link link-with-badge" href="{{route('system.admin.getSupport')}}">
                        <i class="material-icons">message</i>
                        <span class="nav-link-text">Support</span>
                       
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>

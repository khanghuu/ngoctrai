@extends('System.Layouts.Master')

@section('content')
<div class="hk-pg-wrapper">
    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb hk-margin" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">System</a></li>
            <li class="breadcrumb-item " aria-current="page">History</li>
            <li class="breadcrumb-item active" aria-current="page">Wallet</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <div class="col-lg-12">
            <section class="hk-sec-wrapper">
                <h5 class="hk-sec-title text-white">Title</h5>

                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table class="table mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Username</th>
                                            <th>Role</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbody_backgound">
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Jens</td>
                                            <td>Brincker</td>
                                            <td>Brincker123</td>
                                            <td><span class="badge badge-danger">admin</span> </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Mark</td>
                                            <td>Hay</td>
                                            <td>Hay123</td>
                                            <td><span class="badge badge-info">member</span> </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td>Anthony</td>
                                            <td>Davie</td>
                                            <td>Davie123</td>
                                            <td><span class="badge badge-warning">developer</span> </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>David</td>
                                            <td>Perry</td>
                                            <td>Perry123</td>
                                            <td><span class="badge badge-success">supporter</span> </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>Anthony</td>
                                            <td>Davie</td>
                                            <td>Davie123</td>
                                            <td><span class="badge badge-info">member</span> </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">6</th>
                                            <td>Alan</td>
                                            <td>Gilchrist</td>
                                            <td>Gilchrist123</td>
                                            <td><span class="badge badge-success">supporter</span> </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>Pampered by<a href="https://hencework.com/" class="text-dark" target="_blank">Hencework</a> © 2019</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->

</div>
@endsection
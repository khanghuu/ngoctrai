<!DOCTYPE html>
<!--
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: support@hencework.com

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Login</title>
	<base href="{{asset('')}}">
	<meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- Toggles CSS -->
	<link href="vendors/jquery-toggles/css/toggles.css" rel="stylesheet" type="text/css">
	<link href="vendors/jquery-toggles/css/themes/toggles-light.css" rel="stylesheet" type="text/css">

	<!-- Custom CSS -->
	<link href="dist/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
	<!-- Preloader -->
	<div class="preloader-it">
		<div class="loader-pendulums"></div>
	</div>
	<!-- /Preloader -->

	<!-- HK Wrapper -->

	<div class="hk-wrapper">

		<!-- Main Content -->
		<div class="hk-pg-wrapper hk-auth-wrapper">
			<div class="background_login"></div>
			<header class="d-flex justify-content-end align-items-center">

			</header>

			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-12 pa-0">
						<div class="auth-form-wrap pt-xl-0 pt-70">
							<div class="auth-form w-xl-18 w-lg-55 w-sm-75 w-100 border_auth">
								<a class="auth-brand text-center d-block mb-20" href="#">
									<img class="brand-img" src="dist/img/logo/logoBluesea-4.png" alt="brand" />
								</a>
								<div class="auth-content">
									<form>


										<div class="form-group">
											<input class="form-control AuthPage" placeholder="Email" type="email">
										</div>
										<div class="form-group">
											<div class="input-group">
												<input class="form-control AuthPage" placeholder="Password" type="password">
												<div class="input-group-append">
													<span class="input-group-text"><span class="feather-icon"><i data-feather="eye-off"></i></span></span>
												</div>
											</div>
										</div>
										<div class="custom-control custom-checkbox mb-25">
											<input class="custom-control-input" id="same-address" type="checkbox" checked>
											<label class="custom-control-label font-14 text-blue" for="same-address">Keep me logged in</label>
										</div>
										<div>

											<button class="btn btn-primary col-md-5 fleft" type="submit">Login</button>
											<a class="btn btn-primary col-md-5 fleft" href="{{route('getRegister')}}" type="submit">Sign Up</a>
										</div>

										<p class="text-center mt-75"><a href="{{route('getForgotPassword')}}" class="text-white">Forgot Password ?</a></p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Main Content -->

	</div>
	<!-- /HK Wrapper -->

	<!-- JavaScript -->

	<!-- jQuery -->
	<script src="vendors/jquery/dist/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="vendors/popper.js/dist/umd/popper.min.js"></script>
	<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Slimscroll JavaScript -->
	<script src="dist/js/jquery.slimscroll.js"></script>

	<!-- Fancy Dropdown JS -->
	<script src="dist/js/dropdown-bootstrap-extended.js"></script>

	<!-- FeatherIcons JavaScript -->
	<script src="dist/js/feather.min.js"></script>

	<!-- Init JavaScript -->
	<script src="dist/js/init.js"></script>
</body>

</html>
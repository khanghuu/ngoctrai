<!DOCTYPE html>
<!--
Template Name: Brunette - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: support@hencework.com

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Signup</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <!-- Toastr CSS -->
    <link href="vendors/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->
    <!-- HK Wrapper -->
    <div class="hk-wrapper">

        <!-- Main Content -->
        <div class="hk-pg-wrapper hk-auth-wrapper ">
        <div class="background_login"></div>
            <header class="d-flex justify-content-end align-items-center">

            </header>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="auth-form-wrap pt-xl-0 pt-70">
                            <div class="auth-form w-xl-18 w-lg-55 w-sm-75 w-100 border_auth">

                                <a class="auth-brand text-center d-block mb-20" href="#">
                                    <img class="brand-img" src="dist/img/logo/logoBluesea-4.png" alt="brand" />
                                </a>
                                <div class="auth-content">
                                    <form>


                                        <div class="form-group">
                                            <input class="form-control AuthPage" placeholder="Email" type="email">
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input class="form-control AuthPage" placeholder="Password" type="password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><span class="feather-icon"><i data-feather="eye-off"></i></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input class="form-control AuthPage" placeholder="Re Password" type="password">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><span class="feather-icon"><i data-feather="eye-off"></i></span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control AuthPage" placeholder="code" type="number">
                                        </div>
                                        <div class="custom-control custom-checkbox mb-25">
                                            <input class="custom-control-input" id="same-address" type="checkbox" checked>
                                            <label class="custom-control-label font-14 text-blue" for="same-address">
                                                agree terms</label>
                                        </div>
                                        <div class="text-center">

                                            <button class="btn btn-primary col-md-8 " type="submit">Register</button>
                                        </div>

                                        <p class="text-left mb-5 mt-20"><a href="{{route('getLogin')}}" class="text-white"> <i class="fas fa-arrow-alt-circle-left"></i>

                                                Back to login</a></p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Main Content -->

    </div>
    <!-- /HK Wrapper -->
    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="dist/js/jquery.slimscroll.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="dist/js/dropdown-bootstrap-extended.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="dist/js/feather.min.js"></script>

    <!-- Toggles JavaScript -->
    <script src="vendors/jquery-toggles/toggles.min.js"></script>
    <script src="dist/js/toggle-data.js"></script>
    <!-- Toastr JS -->
    <script src="vendors/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <!-- Init JavaScript -->
    <script src="dist/js/init.js"></script>
    <script>
        $(document).ready(function() {
            @if(Session::has('errors'))
            $.toast({
                heading: 'Error!',
                text: "<p>{{ Session::get('errors')->first() }}</p>",
                position: 'top-right',
                loaderBg: '#7a5449',
                class: 'jq-toast-primary',
                hideAfter: 3500,
                stack: 6,
                showHideTransition: 'fade'
            });
            @endif
        });
    </script>


</body>

</html>
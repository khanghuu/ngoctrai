<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserLevel extends Model
{
    protected $table = "user_level";
    
    protected $fillable = ['user_level_ID','user_level_Name', 'user_level_Pecent', 'user_level_Image'];

	public $timestamps = false;
	
	protected $primaryKey = 'user_level_ID';
	
	public function User(){
		return $this->hasMany('App\Model\User', 'User_Agency_Level');
	}
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';

    protected $primaryKey = 'Address_ID';

    protected $fillable = [
        'Address_Currency',
        'Address_Address',
        'Address_User',
        'Address_isUse',
        'Address_Comment'
    ];

    const CREATED_AT = 'Address_CreateAt';
    const UPDATED_AT = 'Address_UpdateAt';
}

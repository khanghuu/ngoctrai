<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function getWallet()
    {
        return view('System.Admin.Wallet');
    }

    public function getInvestment()
    {
        return view('System.Admin.Investment');
    }

    public function getStatistic()
    {
        return view('System.Admin.Statictis');
    }

    public function getKYC()
    {
        return view('System.Admin.Confirm.KYC');
    }

    public function getWithdraw()
    {
        return view('System.Admin.Confirm.Withdraw');
    }

    public function getInterest()
    {
        return view('System.Admin.Confirm.Interest');
    }
    public function getSupport(){
        return view ('System.Admin.support.support');
    }
    public function getDetailsp(){
        return view ('System.Admin.support.Detailsupport');
    }

}

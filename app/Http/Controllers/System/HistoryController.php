<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HistoryController extends Controller
{
    public function getWallet()
    {
        return view('System.History.Wallet');
    }

    public function getCommisson()
    {
        return view('System.History.Commission');
    }

    public function getInvestment()
    {
        return view('System.History.Investment');
    }
}
